package com.bakaeva.tm.api.service;

public interface IAuthService {

    String getUserId();

    void login(String login, String password);

    boolean isAuth();

    boolean isAdmin();

    void logout();

    void registry(String login, String password, String email);

}