package com.bakaeva.tm.command;

import com.bakaeva.tm.api.service.IServiceLocator;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String name();

    public abstract String argument();

    public abstract String description();

    public abstract void execute();

}