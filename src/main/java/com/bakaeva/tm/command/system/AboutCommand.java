package com.bakaeva.tm.command.system;

import com.bakaeva.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String argument() {
        return "-a";
    }

    @Override
    public String description() {
        return "Show developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Alina Bakaeva");
        System.out.println("E-MAIL: bak_al@bk.ru");
    }

}