package com.bakaeva.tm.exception.empty;

public class EmptyUserIdException extends RuntimeException {

    public EmptyUserIdException() {
        super("Error! User ID is empty...");
    }

}