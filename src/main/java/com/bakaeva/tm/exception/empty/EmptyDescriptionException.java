package com.bakaeva.tm.exception.empty;

public class EmptyDescriptionException extends RuntimeException {

    public EmptyDescriptionException() {
        super("Error! Description is empty...");
    }

}