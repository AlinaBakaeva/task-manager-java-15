package com.bakaeva.tm.exception.incorrect;

public class IncorrectIdException extends RuntimeException {

    public IncorrectIdException() {
        super("Error! Id is incorrect...");
    }

}